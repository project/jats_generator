INTRODUCTION
------------

The Journal Article Tag Suite (JATS) is an XML format used to describe scientific literature published online. The JATS provides a set of XML elements and attributes for describing the textual and graphical content of journal articles as well as some non-article material such as letters, editorials, and book and product reviews.

Please consider, that you need some technical knowledge of Drupal to configure this module. Not all settings are done by the administrative UI. Additionally you have to create and edit an include file in the module directory.

You need this if you want to generate a XML file following the JATS schema.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/jats Generator

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/doaj_connector


REQUIREMENTS
------------

This module requires no other modules beside the Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Navigate to the Settings Page (/admin/config/content/jats)

    - Below Content Settings choose the content types on which you would like to enable the JATS functionality. Choose none to enable it on all content types.
    - In the field Good Nodes enabled for download you can enter a comma separated list of nids that should be enabled for public XML download.
    - Below Site-specific Settings you can link to the PHP include file that defines customized settings for your site.


UNINSTALL
---------

Disable and uninstall the module (all variables in the database are going to be completely removed) and delete the module folder from the modules directory.


MAINTAINERS
-----------

Current maintainers:
 * sachbearbeiter - https://www.drupal.org/u/sachbearbeiter
 * gabor_h - https://www.drupal.org/u/gabor_h

This project has been sponsored by:
 * Alexander von Humboldt Institut für Internet und Gesellschaft (HIIG) 
   The Alexander von Humboldt Institute for Internet and Society (HIIG) 
   explores the dynamic relationship between the Internet and society, 
   including the increasing interpenetration of digital infrastructures 
   and various domains of everyday life. Its goal is to understand the 
   interplay of social-cultural, legal, economic and technical norms in 
   the process of digitalisation.
   Visit https://www.hiig.de/en/ for more information.
   This module is used for the Internet Policy Review project.
   It's an open access, fast track and peer-reviewed journal on internet regulation.
   Visit https://policyreview.info/ for more information.