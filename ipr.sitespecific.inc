<?php
/**
 * @file
 *  Site-specific include file for dara_connector.module
 */

function _jats_generator_sitespecific_is_node_ignored($node, &$return) {
  if ($node->nid==364) {
    $return['errors'][]=t('This node contains footnotes, it is ignored for now');
    return TRUE;
  }
  if (($node->nid==121) || ($node->nid==122) || ($node->nid==134)) {
    $return['errors'][]=t('Brackets in references, it is ignored for now');
    return TRUE;
  }
  if (in_array($node->nid, array(101, 102, 105, 112, 120, 180))) {
    $return['errors'][]=t('Old unpublished article, ignored.');
    return TRUE;
  }
  return FALSE;
}